require 'rails_helper'

RSpec.describe Api::NamesController, :api, type: :controller do
  context '#show' do
    let(:user) { create :user }

    it 'returns user\'s name' do
      get :show, params: { user_id: user.id }
      expect(response.status).to eq 200
      expect(JSON.parse(response.body)['name']).to eq user.name
    end

    it 'returns 404 with non-exist user\'s id'  do
      get :show, params: { user_id: 'user.id' }
      expect(response.status).to eq 404
    end
  end
end
