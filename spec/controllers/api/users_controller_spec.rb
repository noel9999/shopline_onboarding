require 'rails_helper'

RSpec.describe Api::UsersController, :api, type: :controller do
  let(:user) { create :user }
    let(:valid_address_params) do
    {
      country:   Faker::Address.country,
      address_1: Faker::Address.street_address,
      address_2: Faker::Address.street_address
    }
  end

  let(:invalid_address_params) { { illagel_key: 'do not store it!' } }


  context '#index' do
    it 'returns a crowd of users' do
      user
      get :index
      expect(response.status).to eq 200
      expect(response).to render_template(:index)
    end
  end

  context '#show' do
    it 'returns a single user' do
      get :show, params: { id: user.id }
      expect(response.status).to eq 200
      expect(response).to render_template(:show)
    end

    it 'returns 404 with invalid user id' do
      get :show, params: { id: 'user.id' }
      expect(response.status).to eq 404
      expect(response).not_to render_template(:show)
    end
  end

  context '#create' do
    let(:valid_user_params) do
      {
        first_name: Faker::Name.first_name,
        last_name:  Faker::Name.last_name,
        gender:     'male',
        age:        40
      }
    end
    let(:invalid_user_params) { { first_name: nil, last_name: nil } }
    it 'creates an user' do
      users_count = User.count
      post :create, params: { user: valid_user_params }
      expect(response.status).to eq(201)
      expect(response).to render_template(:show)
      expect(User.count).to eq (users_count + 1)
      expect(User.last.first_name).to eq valid_user_params[:first_name]
    end

    it 'creates no user with invalid_user_params' do
      users_count = User.count
      post :create, params: { user: invalid_user_params }
      expect(response.status).to eq(422)
      expect(response).not_to render_template(:show)
      expect(User.count).to eq (users_count)
    end

    context 'for address parmas' do
      # it 'create user with valid address params' do
      #   post :create, params: { user: valid_user_params.merge(valid_address_params) }
      #   expect(response.status).to eq(201)
      #   user = User.last
      #   expect(user.address_1).to eq valid_address_params[:address_1]
      #   expect(user.address_2).to eq valid_address_params[:address_2]
      #   expect(user.country).to eq valid_address_params[:country]
      # end

      it 'create user with nested address params' do
        post :create, params: { user: valid_user_params.merge(address: valid_address_params) }
        expect(response.status).to eq(201)
        user = User.last
        expect(user.address).to eq valid_address_params.as_json
        # expect(user.address_1).to eq valid_address_params[:address_1]
        # expect(user.address_2).to eq valid_address_params[:address_2]
        # expect(user.country).to eq valid_address_params[:country]
      end

      it 'does not create invalid address params' do
        address_params = valid_address_params.merge(invalid_address_params)
        post :create, params: { user: valid_user_params.merge(address: address_params) }
        expect(response.status).to eq(201)
        user = User.last
        expect(user.address).to eq valid_address_params.as_json
      end
    end
  end

  context '#update' do
    let(:valid_user_params) { { first_name: Faker::Name.first_name } }
    let(:invalid_user_params) { { first_name: nil } }

    it 'updates the user with valid user params' do
      patch :update, params: { id: user.id, user: valid_user_params }
      expect(response.status).to eq 200
      expect(response).to render_template(:show)
      expect(user.reload.first_name).to eq valid_user_params[:first_name]
    end

    it 'does not update the user with invalid user params' do
      patch :update, params: { id: user.id, user: invalid_user_params }
      expect(response.status).to eq 422
      expect(response).not_to render_template(:show)
      expect(user.reload.first_name).not_to eq invalid_user_params[:first_name]
    end

    context 'for address parmas' do
      let(:address_params) { { country: Faker::Address.country } }
      let(:illegal_address_params) { { bad_attr: Faker::Address.country } }

      # it 'updates address parmas without root node address' do
      #   patch :update, params: { id: user.id, user: address_params }
      #   expect(user.reload.country).to eq address_params[:country]
      # end

      it 'updates address parmas with root node address' do
        patch :update, params: { id: user.id, user: { address: address_params } }
        expect(user.reload.address['country']).to eq address_params[:country]
      end

      it 'does not update illagel_key for address' do
        patch :update, params: { id: user.id, user: { address: illegal_address_params } }
        expect(user.reload.address.keys.map(&:to_s)).not_to include(illegal_address_params.keys.map(&:to_s))
      end
    end
  end

  context '#destroy' do
    it 'destroys the user' do
      user
      user_count = User.count
      delete :destroy, params: { id: user.id }
      expect(response.status).to eq 200
      expect(response).to render_template(:show)
      expect(User.count).to eq(user_count - 1)
    end

    it 'does not destroy any user with invalid user id' do
      user_count = User.count
      delete :destroy, params: { id: 'user.id' }
      expect(response.status).to eq 404
      expect(response).not_to render_template(:show)
      expect(User.count).to eq(user_count)
    end
  end

  context '#count' do
    it 'returns users count' do
      user
      get :count
      expect(response.status).to eq 200
      expect(JSON.parse(response.body)['count']).to eq User.count
    end
  end
end
