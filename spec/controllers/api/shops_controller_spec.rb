require 'rails_helper'

RSpec.describe Api::ShopsController, :api, type: :controller do
  let(:shop) { create :shop }
  let(:user) { create :user }

  describe '#show' do
    it 'returns the shop with correct user id' do
      get :show, params: { user_id: shop.user.id }
      expect(response.status).to eq 200
      expect(response).to render_template :show
    end

    it 'returns the shop with incorrect user id' do
      get :show, params: { user_id: 'shop.user.id' }
      expect(response.status).to eq 404
      expect(response).not_to render_template :show
    end

    it 'returns 404 with uncreated user\'s shop' do
      get :show, params: { user_id: user.id }
      expect(response.status).to eq 404
    end
  end

  describe '#create' do
    let(:shop_params) { { name: Faker::Name.name, description: Faker::GameOfThrones.character } }
    let(:invalid_shop_params) { { description: Faker::GameOfThrones.character } }
    it 'creates user\'s shop with correct params' do
      expect do
        post :create, params: { user_id: user.id, shop: shop_params }
        expect(response.status).to eq 201
        expect(response).to render_template :show
      end.to change { user.shop(true) }.from(nil).to be_a(Shop)
    end

    it 'does not create any shop if it were alread existed' do
      expect do
        post :create, params: { user_id: shop.user.id, shop: shop_params }
        expect(response.status).to eq 409
        expect(response).not_to render_template :show
      end.not_to change { user.shop(true) }
    end

    it 'does not create any shop if params were invalid' do
      expect do
        post :create, params: { user_id: user.id, shop: invalid_shop_params }
        expect(response.status).to eq 422
      end.not_to change { user.shop(true) }
    end
  end

  describe '#update' do
    let(:valid_shop_params) { { name: 'Darth Vader' } }
    let(:invalid_shop_params) { { name: '' } }

    it 'updates a shop with valid shop params' do
      patch :update, params: { user_id: shop.user.id, shop: valid_shop_params }
      expect(response.status).to eq 200
      expect(response).to render_template(:show)
      expect(shop.reload.name).to eq valid_shop_params[:name]
    end

    it 'does not update a shop with invalid shop params' do
      patch :update, params: { user_id: shop.user.id, shop: invalid_shop_params }
      expect(response.status).to eq 422
      expect(shop.reload.name).not_to eq invalid_shop_params[:name]
      expect(response).not_to render_template(:show)
    end

    it 'returns 404 with incorrect user_id' do
      patch :update, params: { user_id: 'shop.user.id', shop: valid_shop_params }
      expect(response.status).to eq 404
      expect(response).not_to render_template(:show)
    end

    it 'returns 404 with uncreated user\'s shop' do
      patch :update, params: { user_id: user.id, shop: valid_shop_params }
      expect(response.status).to eq 404
    end
  end

  describe '#destroy' do
    let(:original_user) { shop.user }

    it 'deletes the users\'s shop if existent' do
      expect do
        delete :destroy, params: { user_id: shop.user.id }
        expect(response.status).to eq 200
        expect(response).to render_template :show
      end.to change { original_user.shop(true) }.from(Shop).to(nil)
    end

    it 'returns 404 if user inexistent' do
      delete :destroy, params: { user_id: 'shop.user.id' }
      expect(response.status).to eq 404
      expect(response).not_to render_template :show
    end

    it 'returns 404 with uncreated user\'s shop' do
      delete :destroy, params: { user_id: user.id }
      expect(response.status).to eq 404
    end
  end
end
