FactoryGirl.define do
  factory :shop do
    name Faker::Name.name
    description Faker::GameOfThrones.character
    user { create :user }
  end
end
