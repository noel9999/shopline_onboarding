# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  age        :integer
#  gender     :string
#  address    :text
#

FactoryGirl.define do
  factory :user do
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    gender User::GENDERS.sample
    age (1..199).to_a.sample
    address do
      {
        country:   Faker::Address.country,
        address_1: Faker::Address.street_address,
        address_2: Faker::Address.street_address
      }
    end
  end
end
