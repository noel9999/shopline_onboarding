# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  age        :integer
#  gender     :string
#  address    :text
#

require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of :first_name }
  it { should validate_presence_of :last_name }
  it { should validate_inclusion_of(:gender).to_allow(User::GENDERS) }
  it { should validate_numericality_of(:age) }
  it { should allow_value(3).for(:age) }
  it { should_not allow_value(-3).for(:age) }

  it { should have_one(:shop) }

  context '#name' do
    it 'returns user\'s full name' do
      user = create :user
      expect(user.name).to eq "#{user.first_name} #{user.last_name}"
    end
  end
end
