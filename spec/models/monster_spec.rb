# == Schema Information
#
# Table name: monsters
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe Monster, type: :model do
  context 'for factory_girls' do
  	it 'should create monster' do
  		expect { create :monster }.to change { Monster.count }.by(1)
  	end

  	it 'should not create monster' do
  		expect { build :monster }.to change { Monster.count }.by(0)
  	end
  end

  context 'for validations' do
  	it { should validate_presence_of(:name) }
  end
end
