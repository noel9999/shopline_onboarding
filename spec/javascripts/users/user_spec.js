describe('Onboarding Task App', function() {

  var host = 'http://localhost:3000'
  var userData = {
    first_name: 'Ryan',
    last_name: 'Giggs',
    age: '44',
    gender: 'male',
    address: {
      country: 'United Kindom',
      address_1: 'Not Knowing',
      address_2: 'No Comment'
    }
  }

  beforeEach(function() {
    browser.get(host);
  });

  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('OnboardingTask');
  });

  it('should create a user', function() {
    var userCount
    // asyc version
    element(by.css('span#user-count')).getText().then(function(text) {
      userCount = Number(text)
    }).then(function() {
      return element(by.id('new-user-button')).click()
    }).then(function() {
      expect(browser.getCurrentUrl()).toEqual('http://localhost:3000/users/new')
      // not knowing why is the hell that even if the last click resovled the new user page still might not be rendered
      browser.driver.sleep(2000);
      return element(by.id('user_first_name')).sendKeys(userData.first_name)
    }).then(function() {
      return element(by.id('user_last_name')).sendKeys(userData.last_name)
    }).then(function() {
      return element(by.id('user_age')).sendKeys(userData.age)
    }).then(function() {
      return element(by.id('user_address_country')).sendKeys(userData.address.country)
    }).then(function() {
      return element(by.id('user_address_address_1')).sendKeys(userData.address.address_1)
    }).then(function() {
      return element(by.id('user_address_address_2')).sendKeys(userData.address.address_2)
    }).then(function() {
      return element(by.css('[type=submit]')).click()
    }).then(function() {
      return element(by.css('span#user-count')).getText()
    }).then(function(text) {
      expect(Number(text)).toEqual(userCount + 1)
    })
  });

  it('should update the user data', function() {
    var newName = 'Super Ryan'
    element.all(by.linkText('Edit')).last().click().then(function() {
      return element(by.model('user.firstName')).clear().sendKeys(newName)
    }).then(function() {
      return element(by.buttonText('Submit')).click()
    }).then(function() {
      return browser.get(host)
    }).then(function() {
      return element.all(by.css('td.user-name')).last().getText()
    }).then(function(text) {
      expect(text).toContain(newName)
    })
  })

  it('should delete the user', function() {
    var userCount
    element(by.css('span#user-count')).getText().then(function(text) {
      userCount = Number(text)
    }).then(function() {
      return element.all(by.linkText('Destroy')).last().click()
    }).then(function() {
      browser.switchTo().alert().accept()
    }).then(function() {
      return element(by.css('span#user-count')).getText()
    }).then(function(text) {
      var newUserCount = Number(text)
      expect(newUserCount).toEqual(userCount - 1)
    })
  })
});
