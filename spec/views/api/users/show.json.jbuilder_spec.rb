require 'rails_helper'

RSpec.describe 'api/users/show.json.jbuilder', type: :view  do
  let(:user) { create :user }
  it 'rendered' do
    assign :user, user
    render
    expect(JSON.parse(rendered)).to eq(
      'id' => user.id.as_json,
      'first_name' => user.first_name,
      'last_name' => user.last_name,
      'address' => user.address.as_json
    )
  end
end