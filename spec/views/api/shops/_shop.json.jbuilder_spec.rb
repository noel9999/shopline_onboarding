require 'rails_helper'

RSpec.describe 'api/shops/_shop.json.jbuilder', type: :view  do
  let(:shop) { create :shop }
  it 'rendered' do
    render 'api/shops/shop', shop: shop
    expect(JSON.parse(rendered)).to eq(
      'id' => shop.id.as_json,
      'name' => shop.name,
      'description' => shop.description
    )
  end
end