RSpec.shared_context 'api', api: true do
  before { request.env["HTTP_ACCEPT"] = 'application/json' }
end

RSpec.configure do |config|
  config.include_context 'api', api: true
end