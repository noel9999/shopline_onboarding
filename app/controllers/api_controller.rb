class ApiController < ActionController::Base
  rescue_from Mongoid::Errors::DocumentNotFound, with: :not_found_handler

  protected

  def not_found_handler(_exception)
    head :not_found
  end
end