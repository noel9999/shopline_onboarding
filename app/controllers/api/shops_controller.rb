class Api::ShopsController < ApiController
  before_action :find_user
  before_action :check_shop

  def show
  end

  def create
    head :conflict and return if @user.shop.present?
    shop = @user.create_shop(shop_params)
    if shop.valid?
      render :show, status: :created
    else
      render json: { errors: shop.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def update
    if @user.shop.update(shop_params)
      render :show, status: :ok
    else
      render json: { errors: @user.shop.errors.full_messages }, status: :unprocessable_entity
    end
  end

  def destroy
    @user.shop.destroy
    render :show
  end

  private

  def find_user
    @user = User.find params[:user_id]
  end

  def check_shop
    case action_name
    when 'create'
      head :conflict and return if @user.shop.present?
    else
      head :not_found and return if @user.shop.blank?
    end
  end

  def shop_params
    params.require(:shop).permit(:name, :description)
  end
end
