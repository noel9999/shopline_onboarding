class Api::NamesController < ApiController
  def show
    user = User.find(params[:user_id])
    render json: { id: user.id, name: user.name }, status: :ok
  end
end
