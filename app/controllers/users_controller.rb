class UsersController < ApplicationController
  before_action :find_user, except: %i(index new create)

  def index
    @users = User.all
  end

  def show
  end

  def edit
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = 'User is created!'
      redirect_to users_path
    else
      render :new, flash: { errors: @user.errors.full_messages }
    end
  end

  def destroy
    @user.destroy
    redirect_to users_path, flash: { success: "User #{@user.name} is deleted!" }
  end

  protected

  def find_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name,
                                 :last_name,
                                 :age,
                                 :gender,
                                 address: User::ADDRESS_ACCESSOR_KEYS)
  end

end
