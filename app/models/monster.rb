# == Schema Information
#
# Table name: monsters
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Monster
  include Mongoid::Document

  field :name, type: String
  field :description, type: String

	validates :name, presence: true
end
