# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  first_name :string
#  last_name  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  age        :integer
#  gender     :string
#  address    :text
#

class User
  include Mongoid::Document
  include Mongoid::Timestamps

  GENDERS = %w(male female others).freeze
  ADDRESS_ACCESSOR_KEYS = %i(country address_1 address_2).freeze

  field :first_name, type: String
  field :last_name,  type: String
  field :age,        type: Integer
  field :gender,     type: String
  field :address,    type: Hash

  validates :first_name, :last_name, presence: true
  validates :age, numericality: { greater_than_or_equal_to: 0 }
  validates :gender, inclusion: { in: GENDERS }

  has_one :shop

  def name
    "#{first_name} #{last_name}"
  end
end
