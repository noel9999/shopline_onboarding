class Shop
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user

  field :name
  field :description

  validates :name, presence: true
end
