app.controller('UsersEditController', ['$scope', '$http', 'userService', function($scope, $http, userService) {
  $scope.renderEditUser = false

  $scope.updateUserForm = function() {
    userService.updateUserInfo($scope.userId, $scope.user)
      .then(function(response) {
        }, function(response) {
            console.log('GG惹')
            console.log(response)
        }
      )
  }

  $scope.emitEditUser = function(event, userId) {
    event.stopPropagation()
    $scope.userId = userId
    $scope.renderEditUser = !$scope.renderEditUser
    if($scope.renderEditUser) {
      userService.getUserInfo(userId)
        .then(function(response) {
            $scope.user = humps.camelizeKeys(response.data);
            $scope.user.name = $scope.user.firstName + ' ' + $scope.user.lastName
          }, function(response) {
            $scope.user = {
              firstName: '',
              lastName: ''
            };
          }
        )
    }
  }
}]);