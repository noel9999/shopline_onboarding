app.factory('userService', ['$http', function($http) {
  return {
    getUserInfo: function(userId) {
      return $http({
        method: 'GET',
        url: '/api/users/' + userId,
        headers: {
          'Accept': 'application/json'
        }
      }) // ng 1.6.3 $http returns a promise
    },

    updateUserInfo: function(userId, userData) {
      return $http({
        method: 'PATCH',
        url: '/api/users/' + userId,
        data: humps.decamelizeKeys(userData, { split: /(?=[A-Z0-9])/ }),
        headers: {
          'Accept': 'application/json'
        }
      })
    }
  }
}])