app.directive('userForm', function(){
  return {
    templateUrl: '/users/user_form.html',
    restrict: 'E'
  }
})