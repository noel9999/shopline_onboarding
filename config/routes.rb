Rails.application.routes.draw do
  namespace :api do
    resources :users, except: %i(new edit) do
      get :count, on: :collection
      resource :name, only: %w(show)
      resource :shop, except: %i(new edit)
    end
  end

  resources :users
  root 'users#index'
end
